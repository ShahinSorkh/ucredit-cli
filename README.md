# UCredit CLI

A simple php cli application to store users and their transactions

## Setup (manual)

- You would need PHP version 8.1 or later.
- Make sure `php-sqlite` is installed.
- Install dependencies:
  ```sh
  $ composer install
  ```
- Initialize database:
  ```sh
  $ ./bin/ucredit db:create
  ```
  _Note_ that database file path can be configured using `DB_DSN` environment
  variable. (see `.env`)
- Run CLI just like any other tool in UNIX environment:
  ```sh
  $ ./bin/ucredit list
  ```

## Setup (docker)

- Build the image:
  ```sh
  $ docker build -t ucredit-cli .
  ```
- Or just pull it from gitlab:
  ```sh
  $ docker pull registry.gitlab.com/shahinsorkh/ucredit-cli:latest
  ```
- Run it:
  - Initialize docker container and database:
    ```sh
    $ docker run --rm -v "$PWD"/mydb.sq3:/app/mydb.sq3 ucredit-cli db:create
    ```
  - Run commands:
    ```sh
    $ docker run --rm -v "$PWD"/mydb.sq3:/app/mydb.sq3 ucredit-cli app:add-user 'John'
    ```

## Commands

- `./bin/ucredit app:add-user`: Adds a new user to the database. Note that
  users' name must be unique.
- `./bin/ucredit app:add-trx`: Adds a new transaction for a specific user. You
  can set a date using `-d` option.
- `./bin/ucredit app:report`: Reports users' total transactions amount in the
  given date. You can filter for a specific user using `-u` option.
