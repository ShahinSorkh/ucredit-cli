<?php

declare(strict_types=1);

namespace ShSo\UCreditCli\Tests;

use League\Container\Container;
use PHPUnit\Framework\TestCase as PHPUnitTestCase;

abstract class TestCase extends PHPUnitTestCase
{
    protected Container $container;

    protected function setUp(): void
    {
        parent::setUp();

        $this->container = new_container();
    }
}
