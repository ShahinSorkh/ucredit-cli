<?php

declare(strict_types=1);

namespace ShSo\UCreditCli\Tests\Providers;

use ShSo\UCreditCli\Tests\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class DatabaseProviderTest extends TestCase
{
    private \PDO $pdo;

    protected function setUp(): void
    {
        parent::setUp();

        $this->pdo = $this->container->get(\PDO::class);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    public function testCanInteractWithDB(): void
    {
        $r = $this->pdo->exec('CREATE TABLE t1 (f1 int, f2 text)');
        self::assertSame(0, $r);

        $r = $this->pdo->exec("INSERT INTO t1 (f1, f2) VALUES (1, 'v1'), (2, 'v2')");
        self::assertSame(2, $r);
    }
}
