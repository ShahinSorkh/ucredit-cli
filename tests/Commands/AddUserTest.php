<?php

declare(strict_types=1);

namespace ShSo\UCreditCli\Tests\Commands;

use Symfony\Component\Console\Tester\CommandTester;

/**
 * @internal
 *
 * @coversNothing
 */
final class AddUserTest extends TestCommand
{
    private CommandTester $cmd;

    protected function setUp(): void
    {
        parent::setUp();

        $this->cmd = new CommandTester($this->app->find('app:add-user'));
    }

    public function testCanAddUser(): void
    {
        $this->cmd->execute(['username' => 'some user']);
        $this->cmd->assertCommandIsSuccessful();
    }
}
