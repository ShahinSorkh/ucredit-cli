<?php

declare(strict_types=1);

namespace ShSo\UCreditCli\Tests\Commands;

use ShSo\UCreditCli\Models\Transaction;
use ShSo\UCreditCli\Models\User;
use ShSo\UCreditCli\Repositories\ReportRepository;
use ShSo\UCreditCli\Repositories\UserRepository;
use ShSo\UCreditCli\Tests\TestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\CommandLoader\ContainerCommandLoader;

abstract class TestCommand extends TestCase
{
    protected Application $app;

    protected function setUp(): void
    {
        parent::setUp();

        $mockedRepo = \Mockery::mock(UserRepository::class);
        $mockedRepo->expects('create')->andReturn(new User(1, 'some user'));
        $mockedRepo->expects('find')->andReturn(new User(1, 'some user'));
        $mockedRepo->expects('addTransaction')->andReturn(new Transaction(1, new \DateTimeImmutable(), 1000));
        $this->container->add(UserRepository::class, $mockedRepo);
        $mockedRepo = \Mockery::mock(ReportRepository::class);
        $mockedRepo->expects('reportUser')->andReturn([]);
        $mockedRepo->expects('reportEveryOne')->andReturn([]);
        $this->container->add(ReportRepository::class, $mockedRepo);

        $this->app = new Application();
        $this->app->setCommandLoader(
            new ContainerCommandLoader($this->container, [
                'app:add-user' => \ShSo\UCreditCli\Commands\AddUser::class,
                'app:add-trx' => \ShSo\UCreditCli\Commands\AddTransaction::class,
                'app:report' => \ShSo\UCreditCli\Commands\Report::class,
            ])
        );
    }
}
