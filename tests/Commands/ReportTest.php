<?php

declare(strict_types=1);

namespace ShSo\UCreditCli\Tests\Commands;

use Symfony\Component\Console\Tester\CommandTester;

/**
 * @internal
 *
 * @coversNothing
 */
final class ReportTest extends TestCommand
{
    private CommandTester $cmd;

    protected function setUp(): void
    {
        parent::setUp();

        $this->cmd = new CommandTester($this->app->find('app:report'));
    }

    public function testCanGetTotalReportForToday(): void
    {
        $this->cmd->execute([]);
        $this->cmd->assertCommandIsSuccessful();
    }

    public function testCanGetTotalReportForYesterday(): void
    {
        $this->cmd->execute([
            'date' => 'yesterday',
        ]);
        $this->cmd->assertCommandIsSuccessful();
    }

    public function testCanGetTotalReportForTodayOfSpecificUser(): void
    {
        $this->cmd->execute([
            '--user-id' => 1,
        ]);
        $this->cmd->assertCommandIsSuccessful();
    }

    public function testCanGetTotalReportForYesterdayOfSpecificUser(): void
    {
        $this->cmd->execute([
            'date' => 'yesterday',
            '--user-id' => 1,
        ]);
        $this->cmd->assertCommandIsSuccessful();
    }
}
