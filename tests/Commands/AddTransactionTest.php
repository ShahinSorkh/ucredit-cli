<?php

declare(strict_types=1);

namespace ShSo\UCreditCli\Tests\Commands;

use Symfony\Component\Console\Tester\CommandTester;

/**
 * @internal
 *
 * @coversNothing
 */
final class AddTransactionTest extends TestCommand
{
    private CommandTester $cmd;

    protected function setUp(): void
    {
        parent::setUp();

        $this->cmd = new CommandTester($this->app->find('app:add-trx'));
    }

    public function testCanAddTransactionForUser(): void
    {
        $this->cmd->execute([
            'user_id' => 1,
            'amount' => 1000,
        ]);
        $this->cmd->assertCommandIsSuccessful();
    }

    public function testCanAddTransactionForUserAtSpecificDate(): void
    {
        $this->cmd->execute([
            'user_id' => 1,
            'amount' => 1000,
            '--date' => 'yesterday',
        ]);
        $this->cmd->assertCommandIsSuccessful();
    }
}
