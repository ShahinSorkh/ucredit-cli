<?php

declare(strict_types=1);

namespace ShSo\UCreditCli\Tests\Repositories;

use ShSo\UCreditCli\Exceptions\UserNameIsDuplicateException;
use ShSo\UCreditCli\Models\User;
use ShSo\UCreditCli\Repositories\UserRepository;
use ShSo\UCreditCli\Tests\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class UserRepositoryTest extends TestCase
{
    private \PDO $pdo;
    private UserRepository $repo;

    protected function setUp(): void
    {
        parent::setUp();
        $this->pdo = $this->container->get(\PDO::class);
        $this->pdo->exec('CREATE TABLE users (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT)');
        $this->pdo->exec('CREATE TABLE transactions (id INTEGER PRIMARY KEY AUTOINCREMENT, date DATETIME, amount INTEGER, user_id INTEGER)');
        $this->pdo->exec("INSERT INTO users (name) VALUES ('me')");
        $this->pdo->exec("INSERT INTO transactions (user_id,date,amount) VALUES (1,'2023-11-28 10:59:00', 1000)");

        $this->repo = $this->container->get(UserRepository::class);
    }

    public function testCanCreateUser(): void
    {
        $user = $this->repo->create('some user');
        self::assertSame(2, $user->id);
        self::assertSame('some user', $user->name);

        $res = $this->pdo->query('SELECT * FROM users')->fetchAll();
        self::assertCount(2, $res);
    }

    public function testCannotCreateUserWithDuplicateName(): void
    {
        $this->expectException(UserNameIsDuplicateException::class);
        $this->repo->create('some user');
        $this->repo->create('some user');
    }

    public function testCanAddTransactionForUser(): void
    {
        $user = new User(1, 'me');
        $trx = $this->repo->addTransaction($user, new \DateTimeImmutable(), -1000);
        self::assertSame(2, $trx->id);
        self::assertSame(-1000, $trx->amount);

        $user_credit = $this->pdo->query('SELECT SUM(amount) FROM transactions WHERE user_id = 1')->fetchColumn();
        self::assertSame(0, $user_credit);
    }
}
