<?php

declare(strict_types=1);

namespace ShSo\UCreditCli\Tests\Repositories;

use ShSo\UCreditCli\Repositories\ReportRepository;
use ShSo\UCreditCli\Tests\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class ReportRepositoryTest extends TestCase
{
    private \PDO $pdo;
    private ReportRepository $repo;

    protected function setUp(): void
    {
        parent::setUp();
        $this->pdo = $this->container->get(\PDO::class);
        $this->pdo->exec('CREATE TABLE users (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT)');
        $this->pdo->exec('CREATE TABLE transactions (id INTEGER PRIMARY KEY AUTOINCREMENT, date DATETIME, amount INTEGER, user_id INTEGER)');
        $this->pdo->exec("INSERT INTO users (name) VALUES ('me')");
        $this->pdo->exec("INSERT INTO users (name) VALUES ('you')");
        $this->pdo->exec("INSERT INTO transactions (user_id,date,amount) VALUES
            (1, '2023-11-26 11:19:00', 1000),
            (1, '2023-11-26 12:29:00', 1000),
            (1, '2023-11-27 13:39:00', 1000),
            (1, '2023-11-28 14:49:00', 1000),
            (1, '2023-11-28 15:59:00', 1000),
            (2, '2023-11-26 11:19:00', 500),
            (2, '2023-11-26 12:29:00', 600),
            (2, '2023-11-27 13:39:00', 1000),
            (2, '2023-11-28 14:49:00', -1000),
            (2, '2023-11-28 15:59:00', 1000)
        ");

        $this->repo = $this->container->get(ReportRepository::class);
    }

    public function testCanReportSpecificUser(): void
    {
        $date = new \DateTimeImmutable('2023-11-28');

        $recs = $this->repo->reportUser($date, 1);

        self::assertCount(1, $recs);
        self::assertSame('2023-11-28', $recs[0]->dateString());
        self::assertSame(2000, $recs[0]->total);
    }

    public function testCanReportEveryOne(): void
    {
        $date = new \DateTimeImmutable('2023-11-28');

        $recs = $this->repo->reportEveryOne($date);

        self::assertCount(2, $recs);
        self::assertSame('2023-11-28', $recs[0]->dateString());
        self::assertSame(2000, $recs[0]->total);
        self::assertSame('2023-11-28', $recs[1]->dateString());
        self::assertSame(0, $recs[1]->total);
    }
}
