<?php

declare(strict_types=1);

namespace ShSo\UCreditCli\Exceptions;

class UserNameIsDuplicateException extends \RuntimeException {}
