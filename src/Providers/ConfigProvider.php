<?php

declare(strict_types=1);

namespace ShSo\UCreditCli\Providers;

use League\Container\ServiceProvider\AbstractServiceProvider;
use Symfony\Component\Dotenv\Dotenv;

class ConfigProvider extends AbstractServiceProvider
{
    private readonly array $config;

    public function __construct()
    {
        $dotenv = new Dotenv();
        $dotenv->loadEnv(__DIR__.'/../../.env');

        $this->config = [
            'db' => [
                'dsn' => $_ENV['DB_DSN'],
                'username' => $_ENV['DB_USERNAME'] ?? null,
                'password' => $_ENV['DB_PASSWORD'] ?? null,
            ],
            'cache' => [
                'path' => $_ENV['CACHE_PATH'] ?? __DIR__.'/../../.cache',
            ],
        ];
    }

    public function provides(string $id): bool
    {
        if (!preg_match('/^config\.\w+$/', $id)) {
            return false;
        }
        [, $key] = explode('.', $id, 2);

        return \array_key_exists($key, $this->config);
    }

    public function register(): void
    {
        foreach ($this->config as $key => $config) {
            $this->getContainer()->addShared("config.{$key}", $config);
        }
    }
}
