<?php

declare(strict_types=1);

namespace ShSo\UCreditCli\Providers;

use Cache\Adapter\Filesystem\FilesystemCachePool;
use League\Container\ServiceProvider\AbstractServiceProvider;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use Psr\SimpleCache\CacheInterface;

class CacheProvider extends AbstractServiceProvider
{
    public function provides(string $id): bool
    {
        return $id === CacheInterface::class;
    }

    public function register(): void
    {
        $this->getContainer()->add(CacheInterface::class, function () {
            /** @var array{path: string} */
            $config = $this->getContainer()->get('config.cache');

            $filesystemAdapter = new Local($config['path']);
            $filesystem = new Filesystem($filesystemAdapter);

            return new FilesystemCachePool($filesystem);
        });
    }
}
