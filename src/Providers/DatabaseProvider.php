<?php

declare(strict_types=1);

namespace ShSo\UCreditCli\Providers;

use League\Container\ServiceProvider\AbstractServiceProvider;

class DatabaseProvider extends AbstractServiceProvider
{
    public function provides(string $id): bool
    {
        return \in_array($id, [
            \PDO::class,
        ], true);
    }

    public function register(): void
    {
        $this->getContainer()->addShared(
            \PDO::class,
            function () {
                /** @var array{dsn: string, username: null|string, password: null|string} */
                $config = $this->getContainer()->get('config.db');

                return new \PDO($config['dsn'], $config['username'], $config['password']);
            }
        );
    }
}
