<?php

declare(strict_types=1);

namespace ShSo\UCreditCli\Commands;

use ShSo\UCreditCli\Repositories\ReportRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:report',
    description: 'Reports what is going on.',
    hidden: false,
)]
class Report extends Command
{
    public function __construct(private readonly ReportRepository $repository)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $date_string = (string) $input->getArgument('date');
        $date = new \DateTimeImmutable($date_string);
        $user_id = (int) $input->getOption('user-id');

        if ($user_id) {
            $recs = $this->repository->reportUser($date, $user_id);
        } else {
            $recs = $this->repository->reportEveryOne($date);
        }

        $output->writeln([
            'Found '.\count($recs).' records',
            '-------------------------------',
        ]);
        foreach ($recs as $rec) {
            $output->writeln(
                "[{$rec->dateString()}] {$rec->username}#{$rec->user_id}\t{$rec->total}"
            );
        }

        return 0;
    }

    protected function configure(): void
    {
        $this
            ->addArgument('date', InputArgument::OPTIONAL, 'Date of the report', 'today')
            ->addOption('user-id', 'u', InputOption::VALUE_REQUIRED, 'Filter report for a specific user')
        ;
    }
}
