<?php

declare(strict_types=1);

namespace ShSo\UCreditCli\Commands;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'db:create',
    description: 'Create database.',
    hidden: false,
)]
class CreateDatabase extends Command
{
    public function __construct(private readonly \PDO $pdo)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // This implementation is for sqlite3 only
        // other RDBMSes might need adjustments..

        $output->writeln('Creating users table..');
        $this->pdo->exec('CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT)');
        $output->writeln('Creating transactions table..');
        $this->pdo->exec('CREATE TABLE IF NOT EXISTS transactions (id INTEGER PRIMARY KEY AUTOINCREMENT, date DATETIME, amount INTEGER, user_id INTEGER)');

        $output->writeln('done');

        return 0;
    }
}
