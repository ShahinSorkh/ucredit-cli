<?php

declare(strict_types=1);

namespace ShSo\UCreditCli\Commands;

use ShSo\UCreditCli\Repositories\UserRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:add-user',
    description: 'Adds a new user.',
    hidden: false,
)]
class AddUser extends Command
{
    public function __construct(private readonly UserRepository $repository)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $username = (string) $input->getArgument('username');
        $user = $this->repository->create($username);

        $output->writeln([
            "User with name '{$user->name}' created successfully",
            "User id is {$user->id}",
        ]);

        return 0;
    }

    protected function configure(): void
    {
        $this->addArgument('username', InputArgument::REQUIRED, 'Name for the user');
    }
}
