<?php

declare(strict_types=1);

namespace ShSo\UCreditCli\Commands;

use ShSo\UCreditCli\Repositories\UserRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:add-trx',
    description: 'Adds a new transaction for a specific user.',
    hidden: false,
)]
class AddTransaction extends Command
{
    public function __construct(private readonly UserRepository $repository)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $date_string = (string) $input->getOption('date');
        $date = new \DateTimeImmutable($date_string);
        $user_id = (int) $input->getArgument('user_id');
        $amount = (int) $input->getArgument('amount');

        if ($amount === 0) {
            $output->writeln('Amount must be a non-zero value');

            return 1;
        }

        $user = $this->repository->find($user_id);
        if (!$user) {
            $output->writeln("Could not find user with id '{$user_id}'");

            return 1;
        }

        $trx = $this->repository->addTransaction($user, $date, $amount);

        $output->writeln([
            "Transaction at {$trx->dateString()} with amount {$trx->amount} added for user '{$user->name}'",
        ]);

        return 0;
    }

    protected function configure(): void
    {
        $this
            ->addArgument('user_id', InputArgument::REQUIRED, 'User ID')
            ->addArgument('amount', InputArgument::REQUIRED, 'Transaction amount')
            ->addOption('date', 'd', InputOption::VALUE_REQUIRED, 'Date of the transaction', 'today')
        ;
    }
}
