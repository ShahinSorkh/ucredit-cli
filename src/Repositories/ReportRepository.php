<?php

declare(strict_types=1);

namespace ShSo\UCreditCli\Repositories;

use Psr\SimpleCache\CacheInterface;
use ShSo\UCreditCli\Models\DailyReport;

class ReportRepository
{
    public function __construct(
        private readonly \PDO $pdo,
        private readonly CacheInterface $cache,
    ) {}

    /** @return DailyReport[] */
    public function reportUser(\DateTimeImmutable $date, int $user_id): array
    {
        $date_string = $date->format('Y-m-d');

        /** @var array{user_id: int, username: string, day_date: string, total: int}[] */
        $recs = $this->pdo->query("
            SELECT u.id AS user_id, u.name AS username, DATE(t.date) AS day_date, SUM(t.amount) AS total
            FROM users AS u
            JOIN transactions AS t
            ON u.id = t.user_id
            WHERE day_date = '{$date_string}' AND u.id = {$user_id}
            GROUP BY DATE(t.date), u.id
        ")->fetchAll(\PDO::FETCH_ASSOC);

        return array_map(
            static fn ($rec) => new DailyReport(
                $rec['user_id'],
                $rec['username'],
                new \DateTimeImmutable($rec['day_date']),
                $rec['total'],
            ),
            $recs,
        );
    }

    /** @return DailyReport[] */
    public function reportEveryOne(\DateTimeImmutable $date): array
    {
        $date_string = $date->format('Y-m-d');
        $cache_key = "reports.{$date->format('Ymd')}";

        if ($this->cache->has($cache_key)) {
            /** @var DailyReport[] */
            $daily_reports = $this->cache->get($cache_key);

            return $daily_reports;
        }

        /** @var array{user_id: int, username: string, day_date: string, total: int}[] */
        $recs = $this->pdo->query("
            SELECT u.id AS user_id, u.name AS username, DATE(t.date) AS day_date, SUM(t.amount) AS total
            FROM users AS u
            JOIN transactions AS t
            ON u.id = t.user_id
            WHERE day_date = '{$date_string}'
            GROUP BY DATE(t.date), u.id
        ")->fetchAll(\PDO::FETCH_ASSOC);

        $daily_reports = array_map(
            static fn ($rec) => new DailyReport(
                $rec['user_id'],
                $rec['username'],
                new \DateTimeImmutable($rec['day_date']),
                $rec['total'],
            ),
            $recs,
        );

        $this->cache->set($cache_key, $daily_reports);

        return $daily_reports;
    }
}
