<?php

declare(strict_types=1);

namespace ShSo\UCreditCli\Repositories;

use Psr\SimpleCache\CacheInterface;
use ShSo\UCreditCli\Exceptions\UserNameIsDuplicateException;
use ShSo\UCreditCli\Models\Transaction;
use ShSo\UCreditCli\Models\User;

class UserRepository
{
    private readonly \PDOStatement $createUserStmt;
    private readonly \PDOStatement $findUserStmt;
    private readonly \PDOStatement $findUserByNameStmt;
    private readonly \PDOStatement $createTrxStmt;

    public function __construct(
        private readonly \PDO $pdo,
        private readonly CacheInterface $cache,
    ) {
        $this->createUserStmt = $this->pdo->prepare('INSERT INTO users (name) VALUES (:name)');
        $this->findUserStmt = $this->pdo->prepare('SELECT * FROM users WHERE id = :id');
        $this->findUserByNameStmt = $this->pdo->prepare('SELECT * FROM users WHERE name = :name');
        $this->createTrxStmt = $this->pdo->prepare('INSERT INTO transactions (user_id, date, amount) VALUES (:user_id, :date, :amount)');
    }

    public function create(string $name): User
    {
        $this->findUserByNameStmt->execute(['name' => $name]);
        $found = $this->findUserByNameStmt->fetchAll();
        if (!empty($found)) {
            throw new UserNameIsDuplicateException();
        }

        $success = $this->createUserStmt->execute(['name' => $name]);
        if (!$success) {
            throw new \PDOException((string) $this->pdo->errorCode());
        }

        $user_id = $this->pdo->lastInsertId();

        return new User((int) $user_id, $name);
    }

    public function addTransaction(User $user, \DateTimeImmutable $date, int $amount): Transaction
    {
        $success = $this->createTrxStmt->execute([
            'user_id' => $user->id,
            'date' => $date->format('Y-m-d H:i:s'),
            'amount' => $amount,
        ]);
        if (!$success) {
            throw new \PDOException((string) $this->pdo->errorCode());
        }

        $trx_id = $this->pdo->lastInsertId();
        $trx = new Transaction((int) $trx_id, $date, $amount);

        $cache_key = "reports.{$date->format('Ymd')}";
        $this->cache->delete($cache_key);

        return $trx;
    }

    public function find(int $id): ?User
    {
        $this->findUserStmt->execute(['id' => $id]);

        /** @var array{id: int, name: string} */
        $rec = $this->findUserStmt->fetch(\PDO::FETCH_ASSOC);

        return new User($id, $rec['name']);
    }
}
