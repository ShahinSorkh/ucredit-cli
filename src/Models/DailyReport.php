<?php

declare(strict_types=1);

namespace ShSo\UCreditCli\Models;

class DailyReport
{
    public function __construct(
        public int $user_id,
        public string $username,
        public \DateTimeImmutable $date,
        public int $total,
    ) {}

    public function dateString(): string
    {
        return $this->date->format('Y-m-d');
    }
}
