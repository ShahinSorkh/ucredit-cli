<?php

declare(strict_types=1);

namespace ShSo\UCreditCli\Models;

class User
{
    public function __construct(
        public int $id,
        public string $name,
    ) {}
}
