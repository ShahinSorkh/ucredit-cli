<?php

declare(strict_types=1);

namespace ShSo\UCreditCli\Models;

class Transaction
{
    public function __construct(
        public int $id,
        public \DateTimeImmutable $date,
        public int $amount,
    ) {}

    public function dateString(): string
    {
        return $this->date->format('Y-m-d H:i:s');
    }
}
