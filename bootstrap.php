<?php

declare(strict_types=1);

use League\Container\Container;
use League\Container\ReflectionContainer;

if (!function_exists('new_container')) {
    function new_container(): Container
    {
        $container = new Container();
        $container->delegate(new ReflectionContainer(true));

        $container
            ->addServiceProvider(
                new ShSo\UCreditCli\Providers\ConfigProvider()
            )
            ->addServiceProvider(
                new ShSo\UCreditCli\Providers\DatabaseProvider()
            )
            ->addServiceProvider(
                new ShSo\UCreditCli\Providers\CacheProvider()
            )
        ;

        return $container;
    }
}
